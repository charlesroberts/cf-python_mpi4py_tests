# Test calculating some variances in parallel
import cf
import os
import time
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

path = '/tmp'

cf.CHUNKSIZE(100000000)
f = cf.read_field(os.path.join(path, '16GB.nc'))

# Time first with read
wt = time.time()
f.Data._var_test()
wt = time.time() - wt

wall_times = comm.gather(wt, root=0)
if rank == 0:
    print 'Maximum time for var_test with read:', max(wall_times)

# Time again without read, but with copy
wt = time.time()
f.Data._var_test()
wt = time.time() - wt

wall_times = comm.gather(wt, root=0)
if rank == 0:
    print 'Maximum time for var_test without read with copy:', max(wall_times)

# Time again without read or copy (by running a third time for the
# time being)
wt = time.time()
f.Data._var_test()
wt = time.time() - wt

wall_times = comm.gather(wt, root=0)
if rank == 0:
    print 'Maximum time for var_test without read without copy:', max(wall_times)
