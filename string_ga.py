import numpy as np

from mpi4py import MPI
from ga4py import ga

nodeid = ga.nodeid()

a = np.array([['Hello', 'World'], ['Orangutan', 'Banana']])

handle = ga.register_dtype(a.dtype)

b = ga.create(handle, a.shape)

if nodeid == 0:
    ga.put(b, a)

ga.sync()

if nodeid == 1:
    c = ga.get(b)
    print('c =', c)
    print('type of c:', type(c))
    print('dtype of c:', c.dtype)

ga.destroy(b)
ga.deregister_type(handle)
