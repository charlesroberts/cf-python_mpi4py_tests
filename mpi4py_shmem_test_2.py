from mpi4py import MPI 
import numpy as np 

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

def create_sma(size, itemsize, dtype):
    '''
    Create shared memory array
    
    Parameters:
        size     : number of elements in the array
        itemsize : size of one element in bytes
        dtype    : numpy datatype
    
    Returns:
        out      : shared memory numpy ndarray
        win      : shared memory window
    '''
    if rank == 0: 
        nbytes = size * itemsize 
    else: 
        nbytes = 0
    #--- End: if
    info = MPI.Info.Create()
    info['no_locks'] = 'true'
    win = MPI.Win.Allocate_shared(nbytes, itemsize, info, comm)
    info.Free()
    saved_itemsize = itemsize
    buf, itemsize = win.Shared_query(0) 
    assert itemsize == saved_itemsize
    buf = np.array(buf, dtype='B', copy=False) 
    out = np.ndarray(buffer=buf, dtype=dtype, shape=(size,))
    return out, win

def create_masked_sma(size):
    '''
    Create a masked shared memory array of type double
    
    Parameters:
        size     : number of elements in the array
    
    Returns:
        out      : shared memory numpy ndarray
        data_win : shared memory window for the data
        mask_win : shared memory window for the mask
    '''
    # Create the data array
    itemsize = MPI.DOUBLE.Get_size()
    dtype = 'd'
    data, data_win = create_sma(size, itemsize, dtype)

    # Create the mask
    itemsize = MPI.C_BOOL.Get_size()
    dtype = '?'
    mask, mask_win = create_sma(size, itemsize, dtype)
    
    # Create a masked array
    out = np.ma.array(data, mask=mask, copy=False)
    return out, data_win, mask_win
    
size = 16

# Create arrays
array_0, data_win_0, mask_win_0 = create_masked_sma(size)
array_1, data_win_1, mask_win_1 = create_masked_sma(size)

# Test array_0
data_win_0.Fence(MPI.MODE_NOPRECEDE)
mask_win_0.Fence(MPI.MODE_NOPRECEDE)
if rank == 0:
    array_0[:] = np.arange(size)
    array_0[:size/2] =  np.ma.masked
data_win_0.Fence()
mask_win_0.Fence()
if rank == 1:
    print array_0
data_win_0.Fence(MPI.MODE_NOSUCCEED)
mask_win_0.Fence(MPI.MODE_NOSUCCEED)

del array_0
data_win_0.Free()
mask_win_0.Free()

# Test array_0
data_win_1.Fence(MPI.MODE_NOPRECEDE)
mask_win_1.Fence(MPI.MODE_NOPRECEDE)
if rank == 1:
    array_1[:] = np.arange(size)
    array_1[size/2:] =  np.ma.masked
data_win_1.Fence()
mask_win_1.Fence()
if rank == 0:
    print array_1
data_win_1.Fence(MPI.MODE_NOSUCCEED)
mask_win_1.Fence(MPI.MODE_NOSUCCEED)

del array_1
data_win_1.Free()
mask_win_1.Free()
