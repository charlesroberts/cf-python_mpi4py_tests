from mpi4py import MPI
import numpy as np

def mysum_obj(a, b):
    b = (a[0] + b[0], a[1] + b[1])
    return b

def mysum_buf(a, b, dt):
    raise ValueError('Buffer operation not supported')

def mysum(ba, bb, dt):
    if dt is None:
        return mysum_obj(ba, bb)
    else:
        return mysum_buf(ba, bb, dt)


myop = MPI.Op.Create(mysum)

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

n = 16
m = 1024**2 / 8
a = (np.ones((n, m)), np.zeros((n, m)))

b = comm.allreduce(a, op=myop)

assert np.allclose(b[0], size)
assert np.allclose(b[1], 0)
