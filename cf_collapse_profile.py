# Test calculating some variances in parallel
import cf
import os
import cProfile
import pstats
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

path = '/tmp'

cf.CHUNKSIZE(100000000)
f = cf.read_field(os.path.join(path, '16GB.nc'))

# Profile first with read
pr = cProfile.Profile()
pr.enable()
print f.collapse('variance').array
pr.disable()

ps = pstats.Stats(pr)
ps.sort_stats('cumulative')
if rank == 0:
    ps.print_stats(0.05)

# Profile again without read
f.Data.to_memory(parallelise=True)
pr = cProfile.Profile()
pr.enable()
print f.collapse('variance').array
pr.disable()

ps = pstats.Stats(pr)
ps.sort_stats('cumulative')
if rank == 0:
    ps.print_stats(0.05)
