import glob
import cf
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

import argparse

parser = argparse.ArgumentParser(description = 'test handling of tempfiles')
parser.add_argument('--datafile', type=str, help='full path of the data file', default='~/Data/cru.nc')
parser.add_argument('--chunksize', type=int, help='cf-python chunk size in bytes', default=1000000)
parser.add_argument('--minncfm', type=int, help='number of chunks of memory to keep free', default=90000)

args = parser.parse_args()

def print_files(command):
    comm.barrier()
    if rank == 0:
        print
        print command
        print
        temp_dirs  = glob.glob('/tmp/cf_array_??????')
        temp_files = glob.glob('/tmp/cf_array_??????/cf_array_??????.npy')
        lock_files = glob.glob('/tmp/cf_array_??????/cf_array_??????.npy_??????')
        print 'Number of temp dirs:',  len(temp_dirs)
        print 'Number of temp files:', len(temp_files)
        print 'Number of lock files:', len(lock_files)
        print
    comm.barrier()

cf.MINNCFM(args.minncfm)
cf.CHUNKSIZE(args.chunksize)

f = cf.read_field(args.datafile)
print_files('f = cf.read_field(args.datafile)')

g = f.collapse('area: mean')
print_files("g = f.collapse('area: mean')")

h = f.collapse('area: mean')
print_files("h = f.collapse('area: mean')")

del h
print_files('del h')

i = g.copy()
del g
print_files('i = g.copy()\ndel g')

del i
print_files('del i')
