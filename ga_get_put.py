from mpi4py import MPI
from ga4py import ga
import numpy as np

nodeid = ga.nodeid()
size = 1000

a = ga.create(ga.C_DBL, size)

if nodeid == 0:
    ga.put(a, np.ones(size))

ga.sync()

if nodeid == 1:
    shape = ga.get(a).shape
    print('Got array on PE', nodeid, 'of shape', shape)

ga.destroy(a)
