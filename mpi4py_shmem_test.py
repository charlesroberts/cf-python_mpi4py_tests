from mpi4py import MPI 
import numpy as np 

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

size = 1000 
itemsize = MPI.DOUBLE.Get_size() 
if rank == 0: 
    nbytes = size * itemsize 
else: 
    nbytes = 0
info = MPI.Info.Create()
info['no_locks'] = 'true'
win = MPI.Win.Allocate_shared(nbytes, itemsize, info, comm)
info.Free()

buf, itemsize = win.Shared_query(0) 
assert itemsize == MPI.DOUBLE.Get_size() 
buf = np.array(buf, dtype='B', copy=False) 
ary = np.ndarray(buffer=buf, dtype='d', shape=(size,)) 

win.Fence(MPI.MODE_NOPRECEDE)
if rank == 1: 
    ary[:5] = np.arange(5) 
win.Fence() 
if rank == 0: 
    print ary[:10]
win.Fence(MPI.MODE_NOSUCCEED)

del ary
win.Free()
