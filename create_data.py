# Create a netCDF file

import os
from netCDF4 import Dataset
import numpy as np

import argparse

parser = argparse.ArgumentParser(description = 'create a netCDF file with random data')
parser.add_argument('--datapath', type=str, help='path where data file will reside', default='/tmp')
parser.add_argument('--datasize', type=int, help='size of data in GB', default=16)
parser.add_argument('--scalefactor', type=int, help='factor to scale axes by', default=1)
parser.add_argument('--masked', type=bool, help = 'whether to create a masked array', default=False)

args = parser.parse_args()

n = args.datasize * args.scalefactor
m = 1024**3 // 8 // args.scalefactor

if args.scalefactor == 1:
    fname = os.path.join(args.datapath, str(n) + 'GB.nc')
else:
    fname = os.path.join(args.datapath, str(args.datasize) + 'GB' +
                         '_' + str(args.scalefactor) + '.nc')
d = Dataset(fname, 'w', fmt='NETCDF4')
d.createDimension('n', n)
d.createDimension('m', m)
v = d.createVariable('data', np.float64, ('n', 'm'))
if args.masked:
    v[...] = np.ma.masked_all((n, m))
    v[0, :] = np.random.rand(m)
else:
    v[...] = np.random.rand(n, m)
d.close()
