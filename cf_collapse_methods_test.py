import cf
import os

import argparse

parser = argparse.ArgumentParser(description = 'test calculating different methods with cf.Field.collapse using mpi4py')
parser.add_argument('--datapath', type=str, help='path where data file resides', default='/tmp')
parser.add_argument('--datasize', type=int, help='size of data in GB', default=16)
parser.add_argument('--chunksize', type=int, help='cf-python chunk size in bytes', default=100000000)

args = parser.parse_args()

cf.CHUNKSIZE(args.chunksize)
f = cf.read_field(os.path.join(args.datapath, str(args.datasize) + 'GB.nc'))

print f.collapse('max').array
print f.collapse('min').array
print f.collapse('mean').array
print f.collapse('mid_range').array
print f.collapse('range').array
print f.collapse('sample_size').array
print f.collapse('sum').array
print f.collapse('var').array
print f.collapse('sd').array
print f.collapse('sum_of_weights').array
print f.collapse('sum_of_weights2').array
