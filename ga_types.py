from mpi4py import MPI
from ga4py import ga

ga_type_handles = [ga.C_CHAR,
                   ga.C_INT,
                   ga.C_LONG,
                   ga.C_FLOAT,
                   ga.C_DBL,
                   ga.C_LDBL,
                   ga.C_SCPL,
                   ga.C_DCPL,
                   ga.C_LDCPL]

to_ga_handle = {}
for handle in ga_type_handles:
    try:
        np_dtype = ga.dtype(handle)
        to_ga_handle[np_dtype] = handle
    except ValueError:
        pass

for k, v in to_ga_handle.iteritems():
    print '{:20}: {}'.format(repr(k), v)

print
print 'no. of handles:', len(ga_type_handles)
print 'no. of dtypes: ', len(to_ga_handle)
print

import numpy as np

a = np.arange(0.0, 10.0, 1.0)

print 'a =', a
print 'dtype of a is:', a.dtype
print 'ga type handle is:', to_ga_handle[a.dtype]
