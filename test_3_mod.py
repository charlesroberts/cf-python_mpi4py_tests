from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

class Partition(object):
    '''A dummy Partition class. Partition n will be processed on only if
    `process` is set to True on a particular process.

    '''
    def __init__(self, n):
        self.n = n
        self.process = False
    #--- End: def
#--- End: class

class Field(object):
    '''A dummy field class containing n_partitions partitions.'''
    def __init__(self, n_partitions=100):
        # Create the same number of partitions on all processes
        self.partition_list = [Partition(i) for i in range(n_partitions)]
        self.n_partitions = n_partitions
        
        # On each process set roughly n_partitions/size partitions to
        # be processed
        for p in self.partition_list:
            if min(p.n / (n_partitions/size), size - 1) == rank:
                p.process = True
            #--- End: if
        #--- End: for
    #--- End: def

    def process(self):
        # Process only those partitions selected for processing on
        # this rank
        processed_partitions = []
        for p in self.partition_list:
            if p.process:
                processed_partitions.append(p)
            #--- End: if
        #--- End: for

        # Gather the processed partition objects to all processes
        partition_list = comm.allgather(processed_partitions)

        # Check the lengths of the partition lists on process 0
        if rank == 0:
            for i, sublist in enumerate(partition_list):
                print i, len(sublist)
            #--- End: for
        #--- End: if
    
        # Flatten partition list
        self.partition_list = [p for sublist in partition_list for p in sublist]

        # Verify that all partitions have been processed
        processed = [False for i in range(self.n_partitions)]
        for p in self.partition_list:
            processed[p.n] = True
        #--- End: for
        for item in processed:
            assert(item)
        #--- End: for
        print 'All partitions processed successfully on process', rank
    #--- End: def
#--- End: class
