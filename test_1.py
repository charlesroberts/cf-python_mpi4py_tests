from mpi4py import MPI

class Partition(object):
    '''A dummy partition class. Partition n will be processed on process
    n. If this is successful `processed` will be set to True.

    '''
    def __init__(self, n):
        self.n = n
        self.processed = False
    #--- End: def
#--- End: class

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# Create the same number of partitions as there are processes on all
# processes
partition_list = [Partition(i) for i in range(size)]

# On each process only process the partion with the same number as the
# rank of the process
for p in partition_list:
    if p.n == rank:
        p.processed = True
    #--- End: if
#--- End: for

# Count the partitions processed on each process
n_processed = 0
for p in partition_list:
    if p.processed:
        n_processed += 1
    #--- End: if
#--- End: for
print n_processed, 'partitions processed on process', rank

# Gather back the partition objects to process 0
partition_list = comm.gather(partition_list[rank], root=0)

# Verify that all partitions have been processed
if rank == 0:
    for p in partition_list:
        assert p.processed
    #--- End: for
    print 'All partitions processed successfully'
#--- End: if
