# Test calculating a grouped collapse in parallel
import cf
import os
import time
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

import argparse

parser = argparse.ArgumentParser(description = 'test calculating a variance with cf.Field.collapse using mpi4py')
parser.add_argument('--datafile', type=str, help='path to data file', default='~/Data/cru.nc')
parser.add_argument('--resultfile', type=str, help='path to result file', default='~/Data/cru_grouped_collapse.nc')
parser.add_argument('--chunksize', type=int, help='cf-python chunk size in bytes', default=1000000)
parser.add_argument('--iterations', type=int, help='number of times to repeat test', default=3)

args = parser.parse_args()

cf.CHUNKSIZE(args.chunksize)

if rank == 0:
    max_wall_times_read = []

for i in range(args.iterations):
    # Time with read
    f = cf.read_field(args.datafile)
    wt = time.time()
    g = f.collapse('T: mean', group=3)
    wt = time.time() - wt
    h = cf.read_field(args.resultfile)
    assert g.equals(h)

    wall_times = comm.gather(wt, root=0)
    if rank == 0:
        print 'Wall times for grouped collapse with read on each process:'
        print '\n'.join(str(wt) for wt in wall_times)
        max_wall_time = max(wall_times)
        max_wall_times_read.append(max_wall_time)
        print 'Maximum time for grouped collapse with read:', max_wall_time
        print

if rank == 0:
    print 'Best time for grouped collapse with read:', min(max_wall_times_read)
