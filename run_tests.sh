#!/bin/bash

echo Running test 1 with 2 processes
mpirun -n 2 python test_1.py

echo Running test 1 with 3 processes
mpirun -n 3 python test_1.py

echo Running test 1 with 4 processes
mpirun -n 4 python test_1.py

echo Running test 1 with 8 processes
mpirun -n 8 python test_1.py

echo Running test 2 with 2 processes
mpirun -n 2 python test_2.py

echo Running test 2 with 3 processes
mpirun -n 3 python test_2.py

echo Running test 2 with 4 processes
mpirun -n 4 python test_2.py

echo Running test 2 with 8 processes
mpirun -n 8 python test_2.py
