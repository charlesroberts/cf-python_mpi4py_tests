from mpi4py import MPI 
import numpy as np 

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

size = 1000 
itemsize = MPI.DOUBLE.Get_size() 
if rank == 0:
    target = np.empty(size, dtype='d')
else:
    target = None
info = MPI.Info.Create()
info['no_locks'] = 'true'
win = MPI.Win.Create(target, itemsize, info, comm)
info.Free()

win.Fence(MPI.MODE_NOPRECEDE)
if rank == 1:
    origin = np.arange(size, dtype='d')
    win.Put(origin, 0)
win.Fence()
if rank == 0: 
    print target[:10]
elif rank == 1:
    del origin
win.Fence(MPI.MODE_NOSUCCEED)

win.Free()
del target
