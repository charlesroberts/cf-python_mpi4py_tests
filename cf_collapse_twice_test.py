# Test calculating a variance with cf.Field.collapse in parallel,
# collapsing along the short axis of a 2D array created with
# create_data.py and then collapsing a second time to a single point.
import cf
import time
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

import argparse

parser = argparse.ArgumentParser(description = 'test calculating a variance along the short axis with cf.Field.collapse and then collapsing a second time to a point using mpi4py')
parser.add_argument('--datafile', type=str, help='path where of data file', default='/tmp/16GB.nc')
parser.add_argument('--chunksize', type=int, help='cf-python chunk size in bytes', default=100000000)
parser.add_argument('--minncfm', type=int, help='number of chunks of memory to keep free', default=10)
parser.add_argument('--iterations', type=int, help='number of times to repeat test', default=3)

args = parser.parse_args()

cf.CHUNKSIZE(args.chunksize)
cf.MINNCFM(args.minncfm)

if rank == 0:
    max_wall_times = []

for i in range(args.iterations):
    f = cf.read_field(args.datafile)
    wt = time.time()
    g = f.collapse('ncdim%n: variance')
    h = g.collapse('variance')
    wt = time.time() - wt
    print h.array

    wall_times = comm.gather(wt, root=0)
    if rank == 0:
        print 'Wall times for variance twice on each process:'
        print '\n'.join(str(wt) for wt in wall_times)
        max_wall_time = max(wall_times)
        max_wall_times.append(max_wall_time)
        print 'Maximum time for variance twice:', max_wall_time
        print

if rank == 0:
    print 'Best time for variance twice:', min(max_wall_times)
