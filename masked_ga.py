# Run with e.g. 'mpirun -n 4' where n >= array_nodeid + 1

import numpy as np

from mpi4py import MPI
from ga4py import ga

nodeid = ga.nodeid()

array_nodeid = 1

size = 10

data_handle = ga.C_DBL
mask_handle = ga.register_dtype(np.dtype(np.bool))

n = array_nodeid + 1
data = ga.create_irreg(data_handle, size, n, [0] * n)
mask = ga.create_irreg(mask_handle, size, n, [0] * n)

if nodeid == array_nodeid:
    a = np.ma.masked_all(size, dtype=ga.dtype(data_handle))
    ga.put(data, a.data)
    ga.put(mask, a.mask)

ga.sync()

if nodeid == array_nodeid:
    a = np.ma.array(ga.access(data), mask=ga.access(mask), copy=False)
    print 'Accessed array on PE', nodeid
    print a
    a[0] = 0
    a[size - 1] = size - 1
    print 'Changed array on PE', nodeid
    print a
    ga.release_update(data)
    ga.release_update(mask)

ga.sync()

if nodeid == array_nodeid + 1:
    a = np.ma.array(ga.get(data), mask=ga.get(mask), copy=False)
    print 'Got array on PE', nodeid
    print a

ga.destroy(data)
ga.destroy(mask)
ga.deregister_type(mask_handle)
