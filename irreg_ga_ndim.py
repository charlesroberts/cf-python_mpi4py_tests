# Run with e.g. 'mpirun -n 4' where n >= array_nodeid + 1

from mpi4py import MPI
from ga4py import ga

nodeid = ga.nodeid()

array_nodeid = 1
ndim = 2

a = ga.create_irreg(ga.C_DBL,                              # datatype
                    [10] * ndim,                           # shape
                    [1] * (ndim - 1) + [array_nodeid + 1], # block
                    [0] * (ndim + array_nodeid))           # map

if nodeid == array_nodeid:
    shape = ga.access(a).shape
    print('Accessed array on PE', nodeid, 'of shape', shape)
    ga.release(a)

if nodeid == array_nodeid + 1:
    b = ga.get(a)
    shape = b.shape
    print('Got array on PE', nodeid, 'of shape', shape)
    b[...] = 1.0
    ga.put(a, b)

ga.sync()

if nodeid == array_nodeid:
    print('Mean of array:', ga.access(a).mean())

ga.destroy(a)
