# Test calculating a variance with cf.Field.collapse in parallel
import cf
import time
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

import argparse

parser = argparse.ArgumentParser(description = 'test calculating a variance with cf.Field.collapse using mpi4py')
parser.add_argument('--datafile', type=str, help='full path of the data file', default='~/Data/cru.nc')
parser.add_argument('--chunksize', type=int, help='cf-python chunk size in bytes', default=1000000)
parser.add_argument('--iterations', type=int, help='number of times to repeat test', default=3)

args = parser.parse_args()

cf.CHUNKSIZE(args.chunksize)

if rank == 0:
    max_wall_times_read = []
    max_wall_times_no_read_1 = []
    max_wall_times_no_read_2 = []

for i in range(args.iterations):
    # Time first with read
    f = cf.read_field(args.datafile)
    print 'Shape of partition matrix:', f.Data.partitions.matrix.shape
    wt = time.time()
    f.collapse('area: variance', weights='area')
    wt = time.time() - wt

    wall_times = comm.gather(wt, root=0)
    if rank == 0:
        print 'Wall times for variance with read on each process:'
        print '\n'.join(str(wt) for wt in wall_times)
        max_wall_time = max(wall_times)
        max_wall_times_read.append(max_wall_time)
        print 'Maximum time for variance with read:', max_wall_time
        print

    # Time again without read
    f.Data.to_memory(parallelise=True)
    wt = time.time()
    f.collapse('area: variance', weights='area')
    wt = time.time() - wt

    wall_times = comm.gather(wt, root=0)
    if rank == 0:
        print 'Wall times for variance without read #1 on each process:'
        print '\n'.join(str(wt) for wt in wall_times)
        max_wall_time = max(wall_times)
        max_wall_times_no_read_1.append(max_wall_time)
        print 'Maximum time for variance without read #1:', max(wall_times)
        print

    # Time again without read
    wt = time.time()
    f.collapse('area: variance', weights='area')
    wt = time.time() - wt

    wall_times = comm.gather(wt, root=0)
    if rank == 0:
        print 'Wall times for variance without read #2 on each process:'
        print '\n'.join(str(wt) for wt in wall_times)
        max_wall_time = max(wall_times)
        max_wall_times_no_read_2.append(max_wall_time)
        print 'Maximum time for variance without read #2:', max(wall_times)
        print

if rank == 0:
    print 'Best time for variance with read:', min(max_wall_times_read)
    print 'Best time for variance without read #1:', min(max_wall_times_no_read_1)
    print 'Best time for variance without read #2:', min(max_wall_times_no_read_2)
