# Run with e.g. 'mpirun -n 2' where n >= 2

from mpi4py import MPI
from ga4py import ga
import numpy as np

nodeid = ga.nodeid()
size = 2 * 1024**3 / 8 # must be even

a = ga.create(ga.C_DBL, size)

if nodeid == 0:
    shape = ga.put(a, np.ones(size/2), lo=0, hi=size/2)

if nodeid == 1:
    shape = ga.put(a, np.ones(size/2), lo=size/2, hi=size)

ga.sync()

if nodeid == 0:
    shape = ga.get(a, lo=0, hi=size/2).shape
    print('Got array on PE', nodeid, 'of shape', shape)

if nodeid == 1:
    shape = ga.get(a, lo=size/2, hi=size).shape
    print('Got array on PE', nodeid, 'of shape', shape)

ga.destroy(a)
